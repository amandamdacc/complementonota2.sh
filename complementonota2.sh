#!/bin/bash

linhas_max=0
arquivo_max=""

linhas=$(wc -l < "$1")
[ $linhas > $linhas_max ] && { linhas_max=$linhas; arquivo_max="$1"; }

linhas=$(wc -l < "$2")
[ $linhas > $linhas_max ] && { linhas_max=$linhas; arquivo_max="$2"; }

linhas=$(wc -l < "$3")
[ $linhas > $linhas_max ] && { linhas_max=$linhas; arquivo_max="$3"; }

linhas=$(wc -l < "$4")
[ $linhas > $linhas_max ] && { linhas_max=$linhas; arquivo_max="$4"; }

[ -n "$arquivo_max" ] && cat "$arquivo_max"
